double cal_cen(TLorentzVector vec,TLorentzVector j0,TLorentzVector j1);
void add()
{
  string list="mc16a.txt";
  ifstream data(list);
  while(!data.eof())
  {
    TString name,name_out;
    data>>name;
    cout<<name<<endl;
    if(name=="")continue;
    double Mly,gam_pt,gam_eta,gam_phi,gam_e,lep_pt,lep_eta,lep_phi,lep_e,lumi,xsec,gen,k,xsb,sow,luminosity,xsection,geneff,kfactor,xsbrfiltereff,sum_of_weights,j0_pt,j0_eta,j0_phi,j0_e,j1_pt,j1_eta,j1_phi,j1_e,dRjj,dRj0gam,dRj1gam,dRj0l,dRj1l,dRlgam,dPhilepmet,dPhij1met,dPhij0met,met,met_eta,met_phi,met_e,jj_pt,jj_eta,jj_phi,jj_e,w_pt,w_eta,w_phi,w_e,wy_cen,dPhiwyjj,dRwyjj,gam_cen,lep_cen,w_cen,dRwy;
    TLorentzVector lep,gam,j0,j1,vmet,w,wy,jj;
    name_out=TString("new")+name;
    TFile *f1=new TFile(name,"READ");
    TFile *fout=new TFile(name_out,"RECREATE");
    TTree *t1=(TTree*)f1->Get("vbswy");
    TTree *t2=(TTree*)f1->Get("DAOD_tree");

//Collect value from the original tree
    t1->SetBranchAddress("lep_pt",&lep_pt);
    t1->SetBranchAddress("lep_eta",&lep_eta);
    t1->SetBranchAddress("lep_phi",&lep_phi);
    t1->SetBranchAddress("lep_e",&lep_e);
    t1->SetBranchAddress("gam_pt",&gam_pt);
    t1->SetBranchAddress("gam_eta",&gam_eta);
    t1->SetBranchAddress("gam_phi",&gam_phi);
    t1->SetBranchAddress("gam_e",&gam_e);
    t1->SetBranchAddress("j0_pt",&j0_pt);
    t1->SetBranchAddress("j0_eta",&j0_eta);
    t1->SetBranchAddress("j0_phi",&j0_phi);
    t1->SetBranchAddress("j0_e",&j0_e);
    t1->SetBranchAddress("j1_pt",&j1_pt);
    t1->SetBranchAddress("j1_eta",&j1_eta);
    t1->SetBranchAddress("j1_phi",&j1_phi);
    t1->SetBranchAddress("j1_e",&j1_e);
    t1->SetBranchAddress("jj_pt",&jj_pt);
    t1->SetBranchAddress("jj_eta",&jj_eta);
    t1->SetBranchAddress("jj_phi",&jj_phi);
    t1->SetBranchAddress("jj_e",&jj_e);
    t1->SetBranchAddress("met",&met);
    t1->SetBranchAddress("met_eta",&met_eta);
    t1->SetBranchAddress("met_phi",&met_phi);
    t1->SetBranchAddress("met_e",&met_e);
    t1->SetBranchAddress("w_pt",&w_pt);
    t1->SetBranchAddress("w_eta",&w_eta);
    t1->SetBranchAddress("w_phi",&w_phi);
    t1->SetBranchAddress("w_e",&w_e);
// Add value from norm tree
    t2->SetBranchAddress("luminosity",&lumi);
    t2->SetBranchAddress("xsection",&xsec);
    t2->SetBranchAddress("geneff",&gen);
    t2->SetBranchAddress("kfactor",&k);
    t2->SetBranchAddress("xsbrfiltereff",&xsb);
    t2->SetBranchAddress("sum_of_weights",&sow);
    
//Add new branch for output tree
    TTree *tout=(TTree*)t1->CloneTree();
    TBranch* b1=tout->Branch("luminosity",&luminosity);
    TBranch* b2=tout->Branch("xsection",&xsection);
    TBranch* b3=tout->Branch("geneff",&geneff);
    TBranch* b4=tout->Branch("kfactor",&kfactor);
    TBranch* b5=tout->Branch("xsbrfiltereff",&xsbrfiltereff);
    TBranch* b6=tout->Branch("sum_of_weights",&sum_of_weights);
    TBranch* b7=tout->Branch("Mly",&Mly);
    TBranch* b8=tout->Branch("dPhilepmet",&dPhilepmet);
    TBranch* b9=tout->Branch("dPhij1met",&dPhij1met);
    TBranch* b10=tout->Branch("dPhij0met",&dPhij0met);
    TBranch* b11=tout->Branch("dRj0gam",&dRj0gam);
    TBranch* b12=tout->Branch("dRj1gam",&dRj1gam);
    TBranch* b13=tout->Branch("dRj0l",&dRj0l);
    TBranch* b14=tout->Branch("dRj1l",&dRj1l);
    TBranch* b15=tout->Branch("dRlgam",&dRlgam);
    TBranch* b16=tout->Branch("wy_cen",&wy_cen);
    TBranch* b17=tout->Branch("dPhiwyjj",&dPhiwyjj);
    TBranch* b18=tout->Branch("dRwyjj",&dRwyjj);
    TBranch* b19=tout->Branch("gam_cen",&gam_cen);
    TBranch* b20=tout->Branch("lep_cen",&lep_cen);
    TBranch* b21=tout->Branch("w_cen",&w_cen);
    TBranch* b22=tout->Branch("dRwy",&dRwy);
//Calculate new elements
    for(int i=0;i<t2->GetEntries();++i)
    {
      t2->GetEntry(i);
      if(i==0)
      {
        luminosity=lumi;xsection=xsec;geneff=gen;kfactor=k;xsbrfiltereff=xsb;
      }
      sum_of_weights+=sow;
    }
    int entries=tout->GetEntries();
    for(int i=0;i<entries;++i)
    {
      t1->GetEntry(i);
      lep.SetPtEtaPhiE(lep_pt,lep_eta,lep_phi,lep_e);
      gam.SetPtEtaPhiE(gam_pt,gam_eta,gam_phi,gam_e);
      j0.SetPtEtaPhiE(j0_pt,j0_eta,j0_phi,j0_e);
      j1.SetPtEtaPhiE(j1_pt,j1_eta,j1_phi,j1_e);
      w.SetPtEtaPhiE(w_pt,w_eta,w_phi,w_e);
      jj.SetPtEtaPhiE(jj_pt,jj_eta,jj_phi,jj_e);
      wy = w + gam; 
      vmet.SetPtEtaPhiE(met,met_eta,met_phi,met_e);
      dPhilepmet = lep.DeltaPhi(vmet);
      dPhij1met = abs(j1.DeltaPhi(vmet));
      dPhij0met = abs(j0.DeltaPhi(vmet));
      dRj0gam = abs(j0.DeltaR(gam));
      dRj1gam = abs(j1.DeltaR(gam));
      dRj0l = abs(j0.DeltaR(lep));
      dRj1l = abs(j1.DeltaR(lep));
      dRlgam = abs(lep.DeltaR(gam));
      Mly = (lep+gam).M();
      wy_cen = cal_cen(wy,j0,j1);
      dPhiwyjj = abs(wy.DeltaPhi(jj));
      dRwyjj = abs(wy.DeltaR(jj));
      gam_cen = cal_cen(gam,j0,j1);
      lep_cen = cal_cen(lep,j0,j1);
      w_cen = cal_cen(w,j0,j1);
      dRwy = abs(w.DeltaR(gam));
      b1->Fill();
      b2->Fill();
      b3->Fill();
      b4->Fill();
      b5->Fill();
      b6->Fill();
      b7->Fill();
      b8->Fill();
      b9->Fill();
      b10->Fill();
      b11->Fill();
      b12->Fill();
      b13->Fill();
      b14->Fill();
      b15->Fill();
      b16->Fill();
      b17->Fill();
      b18->Fill();
      b19->Fill();
      b20->Fill();
      b21->Fill();
      b22->Fill();
    }
    fout->cd();
    tout->Write("",TObject::kOverwrite);
    f1->Close();
    fout->Close();
  }
}
 
double cal_cen(TLorentzVector vec,TLorentzVector j0,TLorentzVector j1)
{
  return abs((vec.Rapidity()-(j0.Rapidity()+j1.Rapidity())/2)/(j0.Rapidity()-j1.Rapidity()));
}
